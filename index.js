#! /usr/bin/env node

const args = process.argv.slice(2)
const rardLaresRepository = 'https://bitbucket.org/eudiasrafael/rard-lares.git'
const currentDir = process.cwd().split('\\').join('/')
const appDir = process.mainModule.filename.split('\\').slice(0, -1).join('/')
const rardLaresAppDir = `${appDir}/rard-lares`

///////////////////////////////////////////////////////////////////////////////////////////////////

const FsExtra = require('fs-extra')
const Exec = require('child_process').exec

///////////////////////////////////////////////////////////////////////////////////////////////////

const positionOfTheParameter = (_param) => {
    return args.indexOf(_param)
}

const parameterValue = (_param) => {
    let pos = positionOfTheParameter(_param)

    while (true) {
        pos++

        if (args[pos] === undefined) break
        if (args[pos].indexOf('-') === 0) continue

        return args[pos]
    }

    return null
}

const message = (_message, _symbol = '> ') => {
    console.log(`${_symbol}${_message}`)
}

const conditionalMessage = (_message, _condition) => {
    if (_condition) {
        message(_message)

        return false
    }

    return true
}

///////////////////////////////////////////////////////////////////////////////////////////////////

const init = () => {
    const directory = parameterValue('init')

    if (!conditionalMessage('Necessário informar o diretório onde instalar', !directory)) return
    if (!conditionalMessage('Diretório informado já existe', FsExtra.existsSync(directory))) return
    
    if (!inCash()) {
        clone(() => init())
    } else {
        if (positionOfTheParameter('--update') >= 0 || positionOfTheParameter('-u') >= 0) {
            cacheUpdate(() => install(directory, () => {
                message('Done!')
            }))
        } else {
            install(directory, () => {
                message('Done!')
            })
        }
    }
}

const update = () => {
    message('Updating...')

    if (inCash()) {
        pull(() => {
            copy('', 'update', () => message('Done!'))
        })
    } else {
        clone(() => copy('', 'update', () => message('Done!')))
    }
}

const cacheUpdate = (_cb = () => { }) => {
    message('Updating...')

    pull(_cb)
}

const help = () => {
    const packageJson = require('./package.json')

    message(`RardLaresCli v${packageJson.version}`, '')
    message('===================================>', '')
    message('', '') 
    message('Commands:', '')
    message('', '')
    message('init :: Starts a new project')
    message('update :: Updates the current project')
    message('', '')
    message('Parameters:', '')
    message('', '')
    message('--update :: Available for the "init" command. Force the update of the Cached ' +
        'RardLares before starting the project.')
    message('-u :: Shortcut to --update.')
    message('', '')
}

///////////////////////////////////////////////////////////////////////////////////////////////////

const inCash = () => {
    return FsExtra.existsSync(rardLaresAppDir)
}

const clone = (_cb = () => { }) => {
    message('Downloading...')

    const SimpleGit = require('simple-git')(appDir)

    SimpleGit.clone(rardLaresRepository, rardLaresAppDir).exec(_cb)
}

const pull = (_cb = () => { }) => {
    const SimpleGit = require('simple-git')(rardLaresAppDir)

    SimpleGit.pull().exec(_cb)
}

const install = (_directory, _cb) => {
    message('Installing...')

    copy(_directory, 'init', () => {
        _cb()
    })
}

const copy = (_dir, _type, _cb) => {
    const dir = `${currentDir}/${_dir}`

    const copyOptions = {
        filter: (_fileName) => {
            const fileName = _fileName.split('\\').join('/')
            const ignoreFiles = [
                `${rardLaresAppDir}/package-lock.json`,
                `${rardLaresAppDir}/README.md`,
                `${rardLaresAppDir}/.git`,
            ]

            if (_type === 'update') {
                ignoreFiles.push(`${rardLaresAppDir}/src`)
            }

            return ignoreFiles.indexOf(fileName) === -1
        }
    }

    FsExtra.copy(rardLaresAppDir, dir, copyOptions, () => {
        if (_type === 'init') {
            npmInstall(_dir, _cb)
        } else {
            FsExtra.copy(`${rardLaresAppDir}/src/bin`, `${dir}/src/bin`, copyOptions, () => {
                npmInstall(_dir, _cb)
            })
        }
    })
}

const npmInstall = (_dir, _cb = () => { }) => {
    message('Installing/Updating dependencies (may take) ...')
    Exec(`cd ${currentDir}/${_dir} && npm install`, (_error, _result) => _cb())
}

///////////////////////////////////////////////////////////////////////////////////////////////////

const RardLaresCli = () => {
    if (positionOfTheParameter('init') >= 0) {
        init()
    } else if (positionOfTheParameter('update') >= 0) {
        update()
    } else if (positionOfTheParameter('cache-update') >= 0) {
        cacheUpdate()
    } else if (positionOfTheParameter('help') >= 0 || positionOfTheParameter('-h') >= 0) {
        help()
    } else {
        message(`"${args[0]}" is not a valid command.`)
        message('Enter "rard-lares-cli -h" for a help.')
        message('', '')
    }
}

RardLaresCli()