# Rard-Lares-Cli

This package allows you to manage a [Rard-Lares](https://bitbucket.org/eudiasrafael/rard-lares).

## Comands

### init

Create a new project [Rard-Lares](https://bitbucket.org/eudiasrafael/rard-lares) in a directory with the specified name.

``` 
rard-lares-cli init myProject
```

### update

Updates the [Rard-Lares](https://bitbucket.org/eudiasrafael/rard-lares) project from the current directory to the latest version.

```
rard-lares-cli update
```

## Parameters

### --update

Available for the "init" command. Force the update of the Cached RardLares before starting the project.

```
rard-lares-cli init --update myProject
```

### -u

Shortcut to --update.

```
rard-lares-cli init -u myProject
```